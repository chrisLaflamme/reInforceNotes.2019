<p align="center">
  <a href="https://next.gatsbyjs.org">
    <img alt="reInforce 2019" src="https://info.capsule8.com/hs-fs/hubfs/Screen%20Shot%202019-05-02%20at%203.38.24%20PM.png?width=1000&name=Screen%20Shot%202019-05-02%20at%203.38.24%20PM.png" width="500" />
  </a>
</p>
<h1 align="center">
</h1>

# Day 1

<p align="center">
  <a href="https://next.gatsbyjs.org">
    <img alt="reInvent" src="https://pbs.twimg.com/media/D5m4TDhWkAUHiYD?format=jpg&name=medium
" width="250" />
  </a>
</p>

## KEY NOTE :key: :speech_balloon: 
  Steve Schmidt

- 50 countries represented at the first re:Inforce conference
- a company should have a good cross regional failover stategy
  - :bulb: audit our stuff to see if we have good distribution across multiple availability zones.
  - :bulb: setup a failover strategy for necessary infrastructure onto a different _region_
- #2 cloud provider has less AZs but also 5x more service downtime than AWS
- :bulb: checkout Config Rules to help "auto" setup Lambda functions that react to certain security related activities. These can be set to automatically remediate found problems.
- :bulb: implement KMS on SQS queues
- Foundational Aspect to Security: The Rule of Least Privilage! Restrict human access!
- Bias towards automation.  Make it repeatable and easy
- :question_mark: Look into AWS Nitro architecture
- setup security by default. Make access an 'opt-in' option, rather than 'opt-out'
- 'Encrypt everything'. For real.  Both in transit as well as at rest.
- Automate detection. But also automate remediation.
- NEW FEATURE: EBS ecryption by default (for no charge)
  - requires opting in on a region by region basis
- AWS Config can track changes to API Gateway as well as the API itself
  - :bulb: check this out
- :bulb: use IAM Access Advisor to see what people / roles ACTUALLY use and need vs. what they have.  Narrow down.  Least privilage.
- Security-As-Code (SaC) as well as Infrastructure-As-Code (IaC)
  - Leverage the cloud to secure the cloud (automatically)
- :SERVICE: Checkout Radar to enforce things like encryption and tagging standards and other standards are adhered to.
- :question: Guard Duty
- the 'one workload per machine model' is no longer really viable.
- important workloads should be continually audited automatically
- NEW FEATURE VPC traffic mirroring
  - :question: Wat?!
- Prefer Lambda functions because the serverless AWS security managements decreases the amount of things you have to care about
- !DevSecOps, we should have security _embedded_ in the development process. DevSecOps should just _be_ the way Ops happens.
- re:Inforce June 16-17 in Houston, TX

## ADVANCED SECURITY AUTOMATION MADE SIMPLE

  Mark Nunnikhoven, Trend Micro

- devs definition of done STOPS when their thing works, not that it's secure, elastic, well architected etc etc
- Use aws cli to see what events were actually used, then filter with ./jq

  ```bash
  $ aws cloudtrail lookup-events ...
  <some return>
  ```

- use ACL's for S3 control rather then policies
- :bulb: create a lambda to be triggered with any S3 bucket policy or ACL is modified
- Only systems should be accessing PRD.  No one should ever login to PRD.  Separate accounts for ENVs.
- CloudWatch Metrics, Events, Logs
- A Lambda can triage by immediately updating a security group to narrow access for either ingress or egress and make the system inaccessible.
- Potenial Thing To Do: Correlate auto-scaling alerts with backend data to detect possible DDoS attacks
- Don't over complicate things. Use what is available with CLI, API, Lambda, etc.

|Question|Answer|
|--|--|
|Read the Well Architected Framework| Ok|
|Could we use Access Advisor vs. CloudTrail for access logging?| |
|should buckets NEVER be public and always use a CloudDistribution with an Origin Access Identity (OAI)| |
|whats that multi account AWS security white paper you referenced called? | |
|What the hell is a security playbook?| |

## SECURING SERVERLESS & CONTAINER SERVICES

- Authorization to trigger a Lambda via API Gateway should happen on the gateway itself
- :idea: write a Lambda to detect any instance of unencrypted data, in transit or at rest
- Bailed

## SECURITY BEST PRACTICES THE WELL-ARCHITECTED WAY

  Ben Potter

- checkout wellarchitectedlabs.com
- apparently there is a well architected tool to assess to see if you're well architected, free to use in the console
  - https://eu-west-1.console.aws.amazon.com/wellarchitected/home?region=eu-west-1#/welcome
- keep people away from data and access to the site
- GuardDuty + play/runbooks * practice gamedays * partners = more sleep
- You can store runbooks/ playbooks as markdown in git so hooks can be used and we can fail related builds that create or reference necessary resources (i.e. security groups) that aren't referenced in the run book.  
  - The idea is that the runbook should have been updated prior to the code checkin.
- Permission Boundaries
  - Look into Permissions Boundaries when giving Devs IAM access
    - :question: is that only for IAM or can it be applied to any service?
- Use access advisor to limit to least used privilage for roles and users
- Checkout AWS Organizations when looking into doing multi accounts
- :question: Is there a reason I shouldn't be using SSM vs. AWS Secrets Manager
- Use a CloudFormation template to create baseline automated detective controls
  - Maybe this: https://wellarchitectedlabs.com/Security/200_Automated_Deployment_of_Detective_Controls/README.html
- :bulb: :question: Does CE or CimSec have some CF template for me to apply that will build automated security detection and/or remediation in our account?
- :bulb: Mandate `npm audit fix` is run, clean, and up-to-date on deployed apps.
- Instances should only have access to the internet via the NAT gateway (which is how we are setup currently anyway)
- Keep people away from data
- 