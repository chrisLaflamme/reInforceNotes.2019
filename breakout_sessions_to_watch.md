<p align="center">
  <a href="https://next.gatsbyjs.org">
    <img alt="Youtube" src="https://cdn2.iconfinder.com/data/icons/social-icons-33/128/Youtube-128.png" width="60" />
  </a>
</p>
<h1 align="center">
</h1>

Breakout Sessions to checkout in YouTube:

|Session|Times at re:Inforce|
|-----|-------|
|SDD304 Deep dive into AWS KMS|?|
|Radar: Validation of remedition...|?|
|SDD202 Create & Customize a Lambda roation function for AWS Secrets Management|?|
|SDD403 Building secure APIs in the cloud|Tues 5pm, Wed 8am|
|SDD303  Using AWS Firewall MGR and WAF to pretect web applications | Tues, 2:30pm, Wed 11:45am|
|SEP401 Security benefits of the Nitro architecture | Wed 12:30, 5:00|
|FND205 IAM at enterprice scale: Patterns and tradeoffs | |
|SDD311 Using AWS WAF to protect against bots and scrapers| |

<h1></h1>