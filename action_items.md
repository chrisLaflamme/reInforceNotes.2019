# Ideas from the conference

## Day 1

### General Ideas

|Session|Item|
|------|:---|
||audit our stuff to see if we have good distribution across multiple availability zones|
||setup a failover strategy for necessary infrastructure onto a different _region_|
||checkout AWS Config Rules to help "auto" setup Lambda functions that react to certain security related activities. These can be set to automatically remediate found problems.|
||

### Concrete Action Items

|Session|Item|Jira Ticket|
|----|:---|---|
|Key Note|||
||Verify we have KMS encryption on all RDS, SQS, etc|TBD|
||Use Access Advisor to narrow down role privilages to only what they are using|TBD|
|Advanced Security Automation Made Simple||
||create a Lambda to be triggered with any S3 bucket policy or ACL is modified|TBD|
|Security Best Practices the Well Architected Way|||
||Run through the free Well Architected tool in AWS to see what can be improved.|TBD|
||Figure out a way to require that `npm audit fix` is run, clean, and up-to-date on deployed apps|TBD|
||Establish and maintain security runbooks / playbooks for breaches (or more realistically reach out to CE or CimSec to see if what the response should be)|TBD|


## Day 2

### Concrete Action Items

|Session|Item|Jira Ticket|
|----|:---|---|
|Your First Compliance As Code|||
||Use AWSLabs prebuilt [AWS Config Rules templates](https://github.com/awslabs/aws-config-rules) to deploy ~12 standard CIS Config rules|TBD|
||Review Trust Advisor output to see what issues need to be addressed|TBD|
||[CS Suite](https://cimpress.slack.com/archives/C5RR20LHE/p1561555484194300) is a free open source too that scans your account and returns neat reports with issues. Look into this.|TBD|
|Threat Hunting in CloudTrail and GuardDuty|||
||Construct a plan for sifting through AWS logs. Document how to do it, do it alot, and get good. There are tools like Splunk that are cool but $$$.  Athena is < $$$ but also < good. Once you know the logs structure you can trigger Lambda based on log data.|TBD|
||Create a separate VCS AWS security account to manage security on the main account. Maybe checkout [Antiope](https://github.com/turnerlabs/antiope) on GitHub.|TBD|
|AWS Organizations and IAM Policies|||
||Refer to the picture I took in order to build out a dev lead policy based on tag values `Lead Dev` `True`|TBD|
|Securing Enterprise Grade Serverless Applications|||
|||TBD|
