<p align="center">
  <a href="https://next.gatsbyjs.org">
    <img alt="Gatsby" src="https://info.capsule8.com/hs-fs/hubfs/Screen%20Shot%202019-05-02%20at%203.38.24%20PM.png?width=1000&name=Screen%20Shot%202019-05-02%20at%203.38.24%20PM.png" width="500" />
  </a>
</p>
<h1 align="center">
</h1>

# Day 2

## YOUR FIRST COMPLIANCE AS CODE

### Using the AWS CLI

- review AWS Trusted Advisor for an pre-setup security audit. 
- checkout: [Building the Largest Repo for Serverless Compliance-As-Code (CaC) on YouTube](https://www.youtube.com/watch?v=VR_4209ewIo)
- AWS Systems Manager: Sessions Manager
  - Allows access to instances without the need for an open SSH port, which is way more secure.
  
  ```bash
  // Output CL commands to a table
  $ aws ec2 describe-instances --output table

  // Use filters with the AWS CLI
  $ aws ec2 describe-instances --query "Reservations[].Instances[].[InstanceId]" --output table

  // 
  $ aws configservice describe-config-rules --query "ConfigRules[].ConfigRuleName"
  ```

- Use AWSLabs prebuilt [AWS Config Rules templates](https://github.com/awslabs/aws-config-rules).

### Security Tools To Use

- [CS Suite](https://cimpress.slack.com/archives/C5RR20LHE/p1561555484194300)
  - Free Open Source security auditing tool.  
  - Gives very deep and well formated security findings when run against your AWS account.
- AWS Trusted Advisor
- AWS Inspector
- AWS Guard Duty
- AWS Well-Architected Tool
- Look into the AWS CIS Benchmark. There is a CloudFormation template that applies a bunch of shit against your account.  There is a good doc that runs you through what's created too.
  - Created config rules will start with CIS
- AWS Security Hub shows Compliance Standards as well
  
:bulb: There are likely costs associated with all the AWS tools.  Since they all kind of do the same thing, try out CS Suite first and run that agains the account.  That should be free (ish).


## THREAT HUNTING IN CLOUDTRAIL & GUARDDUTY

- centralized CloudTrail. 
  - New accounts have a CF template that creats and S3 bucket  
  - dumps CloudTrail logs into that S3 bucket
  - that bucket is ingested.
- CloudTrail keys you should care about:
  ```bash
  "eventName"
  "eventSource"
  "sourceIPAddress"
  "arn"
  ```

- :bulb: I really need to get more comfortable sifting through CloudTrail logs.  What's the best way to do this?  Athena?
- :bulb: create a separate VCS AWS security account to manage security on the main account.
- Antiope (PRONO An-Tie-Oh-Pee) is intended to be an open sourced framework for managing resources across hundreds of AWS Accounts. From a trusted Security Account, Antiope will leverage Cross Account Assume Roles to gather up resource data and store them in an inventory bucket. This bucket can then be index by ELK or your SEIM of choice to provide easy searching of resources across hundreds of AWS accounts.
  - https://github.com/turnerlabs/antiope

## AWS ORGANIZATIONS

- AWS Organizations:
- Dev should have freedom to do what they want but only operate within boundaries
- "NotAction" in IAM policies will exclude those things listed from matching the policy.
- :idea: take the picture and mock out a dev lead policy based on tag values
- tags should be part of the access control strategy and should be included in your protection


## SECURING ENTERPRISE-GRADE SERVERLESS APPLICATIONS
  
  George Mao

- stopping doing `chmod 777`
- Principal: follow the principal of least privilage
- Principal: Defense In Depth
  - security on every layer of your infrastructure
    - WAF Up front
    - :idea: use Cognito to allow access into API Gateway
    - KMS for encryption
- Principal: Clean code
  - Don't store keys/secrets/passwords in code
  - Don't use keys. Uses sts:AssumeRole
  - Lambda function should have a single set of responsibilities. This allows better scaling and making security policies more granular. The function shouldn't do too many things.
- You can setup a WAF rule to block all requests > nKB size
- `$context.identity.[<things>]` Lambda function can check the context object that is passed to it from API Gateway to know if it should do the thing requested
- :+1: Attach roles to the Lambda that controls actions it can perform and also defines which resources it can interact with.
- :+1: use ENV_VARs on your Lambda
- Only allow SSL connections to DBs like RDS and Dynamo
- AWS Config allows tracking of any configuration changes to any resource
- Use AWS Config to audit environments
- Don't allow wildcards in policies
- HTTPS is not secure: never trust user input
  - validate, encode, and escape HTML

